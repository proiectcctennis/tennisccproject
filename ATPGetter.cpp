#include "ATPGetter.h"
#include <cpprest/http_client.h>
#include <cpprest/filestream.h>

using namespace utility;                    
using namespace web;                        
using namespace web::http;                 
using namespace web::http::client;          
using namespace concurrency::streams;       


void ATPGetter::getATPstats()
{
	auto fileStream = std::make_shared<ostream>();

	pplx::task<void> requestTask = fstream::open_ostream(U("dataATP.txt")).then([=](ostream outFile)
	{
		*fileStream = outFile;

		http_client client(U("http://www.tennis.com/stats/ATP/"));

		uri_builder builder(U(""));
		return client.request(methods::GET, builder.to_string());
	})

		.then([=](http_response response)
	{
		printf("Received response status code:%u\n", response.status_code());

		return response.body().read_to_end(fileStream->streambuf());
	})

		.then([=](size_t)
	{
		return fileStream->close();
	});

	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		printf("Error exception:%s\n", e.what());
	}
}
