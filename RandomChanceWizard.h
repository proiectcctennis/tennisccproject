#pragma once
#include "Player.h"
#include"Ranking.h"
#include<algorithm>
#include <random>
#include <chrono>

class RandomChanceWizard
{
public:
	RandomChanceWizard(const std::vector<Player>& pl);
	void RankPlayers();
	void AddChance();
	std::string returnNewOrderByChance();

private:
	std::vector<Player> players;
};

