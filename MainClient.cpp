#include <cpprest/http_client.h>
#include <cpprest/json.h>
#include<cstdio>
#include<json.hpp>
#include<sstream>

using namespace web;
using namespace web::http;
using namespace web::http::client;

#include <iostream>
using namespace std;

void FantasyLeague(std::vector<std::string> players, std::vector<int> powerValue) {
	
	for (int i = 0; i < 3; ++i) {
		std::cout << std::endl << "Round " << i + 1 << ": The winners are:" << std::endl << std::endl;
		for (int index = 0; index <= players.size() - 2; index = index + 2) {
			if (powerValue[index] > powerValue[index + 1]) {
				std::cout << players[index] << std::endl;
				players[index + 1] = "#";
			}
			else {
				std::cout << players[index + 1] << std::endl;
				players[index] = "#";
			}
		}
		players.erase(std::remove(players.begin(), players.end(), "#"), players.end());
	}

	std::cout << "THE WINNER IS " << players[0] << std::endl;
}

void display_json(
	json::value const & jvalue,
	int option)
{
	web::json::value text;
	istringstream ss;
	std::string data, buffername, bufferPWstring, bufferIndexStr;
	std::vector<std::string> indexStrPlayer(8, "#");
	std::vector<std::string> namePlayer(8, "#");
	int indexPlayer1, indexPlayer2;
	std::vector<int>powerValuePlayer(8);

	switch (option) {
	case 1:		//print splash
		text = jvalue.at(U("printSplash"));
		wcout << text << endl;
		break;
	case 2:		//latest tennis results
		text = jvalue.at(U("getLastResults"));
		std::wcout << text.as_string() << std::endl;
		break;
	case 3:		//get top atp
		text = jvalue.at(U("requestATPTop100"));
		std::wcout << text.as_string() << std::endl;
		break;
	case 4:		//get top wta
		text = jvalue.at(U("requestWTATop100"));
		std::wcout << text.as_string() << std::endl;
		break;
	case 5:		//predict outcome between 2 atp players
		text = jvalue.at(U("requestRandomChanceATP"));
		std::cout << "Pick the ATP players 1 and 2 by index:" << std::endl;
		std::cin >> indexPlayer1 >> indexPlayer2;
		system("cls");
		data = utility::conversions::to_utf8string(text.to_string());
		ss.str(data);

		while (ss >> bufferIndexStr >> bufferPWstring >> buffername) {
			bufferIndexStr.erase(std::remove(bufferIndexStr.begin(), bufferIndexStr.end(), '"'), bufferIndexStr.end());
			
			if (std::stoi(bufferIndexStr) == indexPlayer1) {
				powerValuePlayer[0] = std::stoi(bufferPWstring);
				std::replace(buffername.begin(), buffername.end(), '_', ' ');
				namePlayer[0] = buffername;
			}
			else if (std::stoi(bufferIndexStr) == indexPlayer2) {
				powerValuePlayer[1] = std::stoi(bufferPWstring);
				std::replace(buffername.begin(), buffername.end(), '_', ' ');
				namePlayer[1] = buffername;
			}
			if (namePlayer[0] != "#" && namePlayer[1] != "#")
				break;
		}

		if (powerValuePlayer[0] > powerValuePlayer[1])
			std::cout << namePlayer[0] << " won the game against " << namePlayer[1] << std::endl << std::endl;
		else if (powerValuePlayer[0] < powerValuePlayer[1])
			std::cout << namePlayer[1] << " won the game against " << namePlayer[0] << std::endl << std::endl;
		else
			std::cout << "It's a tie between " << namePlayer[0] << " and " << namePlayer[1] << std::endl << std::endl;
		break;
	case 6:		//predict outcome between 2 wta players
		text = jvalue.at(U("requestRandomChanceWTA"));
		std::cout << "Pick the WTA players 1 and 2 by index:" << std::endl;
		std::cin >> indexPlayer1 >> indexPlayer2;
		system("cls");
		data = utility::conversions::to_utf8string(text.to_string());
		ss.str(data);
		while (ss >> bufferIndexStr >> bufferPWstring >> buffername) {
			bufferIndexStr.erase(std::remove(bufferIndexStr.begin(), bufferIndexStr.end(), '"'), bufferIndexStr.end());
			
			if (std::stoi(bufferIndexStr) == indexPlayer1) {
				powerValuePlayer[0] = std::stoi(bufferPWstring);
				std::replace(buffername.begin(), buffername.end(), '_', ' ');
				namePlayer[0] = buffername;
			}
			else if (std::stoi(bufferIndexStr) == indexPlayer2) {
				powerValuePlayer[1] = std::stoi(bufferPWstring);
				std::replace(buffername.begin(), buffername.end(), '_', ' ');
				namePlayer[1] = buffername;
			}
			if (namePlayer[0] != "#" && namePlayer[1] != "#")
				break;
		}

		if (powerValuePlayer[0] > powerValuePlayer[1])
			std::cout << namePlayer[0] << " won the game against " << namePlayer[1] << std::endl << std::endl;
		else if (powerValuePlayer[0] < powerValuePlayer[1])
			std::cout << namePlayer[1] << " won the game against " << namePlayer[0] << std::endl << std::endl;
		else
			std::cout << "It's a tie between " << namePlayer[0] << " and " << namePlayer[1] << std::endl << std::endl;
		break;
		break;
	case 7:		//fantasy league
		std::cout << "WELCOME TO FANTASY LEAGUE!" << std::endl;
		std::cout << "You can choose any of the ATP or WTA players in a pick 8 challenge" << std::endl;
		std::cout << "The winner will be determined by your choices and a little bit of luck ;)" << std::endl;
		std::cout << "Alright! First thing first, pick your players!" << std::endl;
		std::cout << "For example, an ATP player with index 5 will be picked as a5\nand a WTA player with index 8 will be w8" << std::endl;
		std::cout << "The ATP players:" << std::endl;
		text = jvalue.at(U("requestATPTop100"));
		std::wcout << text.as_string() << std::endl;
		std::cout << "##############\nThe WTA players:" << std::endl;
		text = jvalue.at(U("requestWTATop100"));
		std::wcout << text.as_string() << std::endl;
		std::cout << "Your choices: ";
		std::cin >> indexStrPlayer[0] >> indexStrPlayer[1] >> indexStrPlayer[2] >> indexStrPlayer[3] >> 
			indexStrPlayer[4] >> indexStrPlayer[5] >> indexStrPlayer[6] >> indexStrPlayer[7];
		system("cls");
		for (int i = 0; i < 8; ++i) {
			int bufferPowerValue;
			if (indexStrPlayer[i][0] == 'a')
			{
				text = jvalue.at(U("requestRandomChanceATP"));	
				data = utility::conversions::to_utf8string(text.to_string());
				ss.str(data);

				indexStrPlayer[i] = indexStrPlayer[i].substr(1);
				int bufferIndex = std::stoi((indexStrPlayer[i]));

				while (ss >> bufferIndexStr >> bufferPWstring >> buffername) {
					bufferIndexStr.erase(std::remove(bufferIndexStr.begin(), bufferIndexStr.end(), '"'), bufferIndexStr.end());

					if (std::stoi(bufferIndexStr) == bufferIndex) {
						bufferPowerValue = std::stoi(bufferPWstring);
						std::replace(buffername.begin(), buffername.end(), '_', ' ');
						break;
					}
				}
			}
			else if (indexStrPlayer[i][0] == 'w')
			{
				text = jvalue.at(U("requestRandomChanceWTA"));	//first index is the chosen one
				data = utility::conversions::to_utf8string(text.to_string());
				ss.str(data);

				indexStrPlayer[i] = indexStrPlayer[i].substr(1);
				int bufferIndex = std::stoi((indexStrPlayer[i]));

				while (ss >> bufferIndexStr >> bufferPWstring >> buffername) {
					bufferIndexStr.erase(std::remove(bufferIndexStr.begin(), bufferIndexStr.end(), '"'), bufferIndexStr.end());

					if (std::stoi(bufferIndexStr) == bufferIndex) {
						bufferPowerValue = std::stoi(bufferPWstring);
						std::replace(buffername.begin(), buffername.end(), '_', ' ');
						break;
					}
				}
			}
			namePlayer[i] = buffername;
			powerValuePlayer[i] = bufferPowerValue;
		}
		FantasyLeague(namePlayer, powerValuePlayer);
		break;
	case 8:	//rising star ATP
		std::cout << "The ATP rising star is ";
		text = jvalue.at(U("requestRStarATP"));
		wcout << text << endl;
		break;
	case 9:	//rising star WTA
		std::cout << "The WTA rising star is ";
		text = jvalue.at(U("requestRStarWTA"));
		wcout << text << endl;
		break;
	default:
		break;
	}
}

pplx::task<http_response> make_task_request(
	http_client & client,
	method mtd,
	json::value const & jvalue)
{
	return (mtd == methods::GET || mtd == methods::HEAD) ?
		client.request(mtd, L"/tennisClient") :
		client.request(mtd, L"/tennisClient", jvalue);
}

void make_request(
	http_client & client,
	method mtd,
	json::value const & jvalue, json::value &returnValue
)
{
	make_task_request(client, mtd, jvalue)
		.then([](http_response response)
	{
		if (response.status_code() == status_codes::OK)
		{
			return response.extract_json();
		}
		return pplx::task_from_result(json::value());
	})
		.then([&](pplx::task<json::value> previousTask)
	{
		try
		{
			returnValue = previousTask.get();
		}
		catch (http_exception const & e)
		{
			wcout << e.what() << endl;
		}
	})
		.wait();
}

int main()
{
	http_client client(U("http://localhost"));
	auto nullvalue = json::value::null();
	

	int option, indexPlayer1, indexPlayer2;
	bool menuLoop = true;

	json::value getvalue;
	make_request(client, methods::GET, nullvalue, getvalue);
	//auto putvalue = json::value::object();

	display_json(getvalue, 1);	//just prints the splash title

	while (menuLoop) {
		std::cout << "Insert 0 to exit" << std::endl;
		std::cout << "Insert 1 to get the latest world Tennis match results" << std::endl;
		std::cout << "Insert 2 to get the top 100 ATP players (server calculation)" << std::endl;
		std::cout << "Insert 3 to get the top 100 WTA players (server calculation)" << std::endl;
		std::cout << "Insert 4 to get the possible outcome in a match between 2 ATP players" << std::endl;
		std::cout << "Insert 5 to get the possible outcome in a match between 2 WTA players" << std::endl;
		std::cout << "Insert 6 to play Fantasy League!" << std::endl;
		std::cout << "Insert 7 to get the ATP Rising Star!" << std::endl;
		std::cout << "Insert 8 to get the WTA Rising Star!" << std::endl;
		std::cin >> option;

		switch (option) {
		case 0:
			system("cls");
			std::cout << "Press any key..." << std::endl;
			menuLoop = false;
			break;
		case 1:
			system("cls");
			display_json(getvalue, 2);
			break;
		case 2:
			system("cls");
			display_json(getvalue, 3);
			break;
		case 3:
			system("cls");
			display_json(getvalue, 4);
			break;
		case 4:
			system("cls");
			display_json(getvalue, 3);
			display_json(getvalue, 5);
			break;
		case 5:
			system("cls");
			display_json(getvalue, 4);
			display_json(getvalue, 6);
			break;
		case 6:
			system("cls");
			display_json(getvalue, 7);
			break;
		case 7:
			system("cls");
			display_json(getvalue, 8);
			break;
		case 8:
			system("cls");
			display_json(getvalue, 9);
			break;
		default:
			system("cls");
			std::cout << "Invalid option!" << std::endl;
			break;
		}
	}

	return 0;
}
