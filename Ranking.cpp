#include "Ranking.h"

Ranking::Ranking(const std::vector<Player>& players)
{
	this->players = players;
}

Ranking::~Ranking()
{
	players.clear();
}

std::vector<Player> Ranking::GetPlayers()
{
	return players;
}

void Ranking::Sort(std::vector<Player>& players)
{
	for (int i = 0; i < players.size() - 1; i++)
	{
		for (int j = i + 1; j < players.size(); j++)
		{
			if (players[i] < players[j])
			{
				Player aux = players[i];
				players[i] = players[j];
				players[j] = aux;
			}
		}
	}
}

std::vector<Player> Ranking::GetSortedPlayers(const std::string& opt)
{
	std::vector<Player> auxPlayers = players;
	int option = -1;

	if (opt == "Single" || opt == "single")
	{
		option = 0;
	}
	else if (opt == "Double" || opt == "double")
	{
		option = 1;
	}
	else if (opt == "Total" || opt == "total")
	{
		option = 2;
	}
	
	switch (option) 
	{
	case 0:
		for (int i = 0; i < auxPlayers.size(); i++)
		{
			auxPlayers[i].SetSinglesSelectedTrue();
		}

		Sort(auxPlayers);
		break;

	case 1:
		for (int i = 0; i < auxPlayers.size(); i++)
		{
			auxPlayers[i].SetDoublesSelectedTrue();
		}

		Sort(auxPlayers);
		break;

	case 2:
		for (int i = 0; i < auxPlayers.size(); i++)
		{
			auxPlayers[i].SetTotalSelectedTrue();
		}

		Sort(auxPlayers);
		break;

	default:
		throw "Error! Please choose an existing score method!";
		break;
	}

	return auxPlayers;
}

std::string Ranking::GetSortedPlayersAsString(const std::string & opt)
{
	std::vector<Player> players = GetSortedPlayers(opt);
	std::vector<Player>::iterator player;
	std::string response = "";
	int rank = 1;

	for (player = players.begin(); player != players.end(); player++) {
		response += "\n#" + std::to_string(rank);
		response += " " + (*player).GetName() + "\n";
		rank++;
	}

	return response;
}
