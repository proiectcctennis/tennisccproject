#pragma once
#include"Player.h"
#include<vector>
#include<memory>

class Match
{
public:
	void setPlayer1(Player &pl1);
	void setPlayer2(Player &pl2);

private:
	std::shared_ptr<Player> player1, player2;
	std::vector<int> player1Score, player2Score;
};

