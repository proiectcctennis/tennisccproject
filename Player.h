#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <array>

// constante la care m-am gandit pentru 'fluidizarea' punctajului
const float SINGLE_POINTS_CALCULUS = 12.53;
const float DOUBLE_POINTS_CALCULUS = 11.51;

// numarul de puncte cu care incepe fiecare jucator; am declarat-o in ideea ca daca un jucator are mai multe lose-uri decat win-uri, punctajul sa nu fie pe minus;
const int STARTING_POINTS = 1000;

class Player
{
private:
	std::string name;
	float totalPoints, singlesPoints, doublesPoints;
	int singlesWin, singlesLoss, doublesWin, doublesLoss, index;
	bool singlesSelected, doublesSelected, totalSelected;

public:
	// cand ne definim un player, il vom instantia MEREU cu datele referitoare la numarul de win-uri, lose-uri la single si double, si ca vom calcula direct in constructor punctele pentru single, double si total
	Player(const std::string n, const int sw, const int sl, const int dw, const int dl);
	~Player();

public:
	void SetSinglesSelectedTrue();
	void SetDoublesSelectedTrue();
	void SetTotalSelectedTrue();
	void SetIndex(int ind);

public:
	void IncrementSinglesWin(int sw);
	void IncrementSinglesLoss(int sl);
	void IncrementDoublesWin(int dw);
	void IncrementDoublesLoss(int dl);

public:
	int GetSinglesWin();
	int GetSinglesLoss();
	int GetDoublesWin();
	int GetDoublesLoss();
	int GetTotalWin();
	int GetTotalLoss();
	int GetIndex();

public:
	std::string GetName();
	float GetSinglesPoints();
	float GetDoublesPoints();
	float GetTotalPoints();

public:
	bool operator <(const Player& player);
	bool operator >(const Player& player);

private:
	float CalculateSinglesPoints();
	float CalculateDoublePoints();
	float CalculateTotalPoints();
};