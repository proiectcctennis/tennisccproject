#include "RandomChanceWizard.h"

RandomChanceWizard::RandomChanceWizard(const std::vector<Player>& pl): players(pl)
{
}

void RandomChanceWizard::RankPlayers()
{
	Ranking ranker(players);
	players = ranker.GetSortedPlayers("Total");

	std::vector<Player>::iterator player;
	int index = 1;

	for (player = players.begin(); player != players.end(); player++) {
		(*player).SetIndex(index);
		index++;
	}
}

void RandomChanceWizard::AddChance()
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(players.begin(), players.end(), std::default_random_engine(seed));
}

std::string RandomChanceWizard::returnNewOrderByChance()
{
	int rank = 1;
	std::string response = "", name;
	std::vector<Player>::iterator player;

	for (player = players.begin(); player != players.end(); player++) {
		response += std::to_string((*player).GetIndex());
		response += " ";
		response += std::to_string(rank%10 + (int)(*player).GetTotalPoints());
		name = (*player).GetName();
		std::replace(name.begin(), name.end(), ' ', '_');
		response += " " + name;
		response += " ";
		rank++;
	}

	return response;
}
