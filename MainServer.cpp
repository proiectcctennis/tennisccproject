/*
	MENTIUNE: SERVERUL TREBUIE RULAT CA ADMIN PT A ASCULTA IN AFARA localhost
*/

#include"TennisLiveGetter.h"
#include"ParserTennisLiveResults.h"
#include"WTAGetter.h"
#include"ATPGetter.h"
#include"ParserATP.h"
#include"ParserWTA.h"
#include"Player.h"
#include"Ranking.h"
#include"RandomChanceWizard.h"
#include<json.hpp>	//din Nlohmann, pt facilitare creare jsonuri cu scop salvare date local 
#include <map>
#include <set>

#define TRACE(msg)            std::wcout << msg
#define TRACE_ACTION(a, k, v) std::wcout << a << L" (" << k << L", " << v << L")\n"

std::string getTop100ATPplayers() {
	ATPGetter atp;
	ParserATP pAtp;
	std::vector<Player> ATPplayers;
	std::string result;

	pAtp.parsareDate();
	atp.getATPstats();

	std::ifstream ATPjson("ATP.json");
	nlohmann::json playerATP;

	while (true)
	{
		ATPjson >> playerATP;

		auto name = playerATP["name"];
		auto singelsWin = playerATP["singelsWins"];
		auto singelsLoss = playerATP["singelsLoss"];
		auto doubleWin = playerATP["doublesWins"];
		auto doubleLoss = playerATP["doublesLoss"];

		Player playerATPobj(name, singelsWin, singelsLoss, doubleWin, doubleLoss);
		ATPplayers.push_back(playerATPobj);

		if (playerATP["index"] == "100")
			break;
	}

	ATPjson.close();
	Ranking rankerATP(ATPplayers);

	result = rankerATP.GetSortedPlayersAsString("Total");
	return result;
}

std::string getTop100WTAplayers() {
	std::string result;
	WTAGetter wta;
	ParserWTA pWta;
	wta.getWTAstats();
	pWta.parsareDate();

	std::vector<Player> WTAplayers;
	std::ifstream WTAjson("WTA.json");
	nlohmann::json playerWTA;

	while (true)
	{
		WTAjson >> playerWTA;

		auto name = playerWTA["name"];
		auto singelsWin = playerWTA["singelsWins"];
		auto singelsLoss = playerWTA["singelsLoss"];
		auto doubleWin = playerWTA["doublesWins"];
		auto doubleLoss = playerWTA["doublesLoss"];

		Player playerWTAobj(name, singelsWin, singelsLoss, doubleWin, doubleLoss);
		WTAplayers.push_back(playerWTAobj);

		if (playerWTA["index"] == "100")
			break;
	}

	WTAjson.close();

	Ranking rankerWTA(WTAplayers);

	result = rankerWTA.GetSortedPlayersAsString("Total");
	return result;
}

std::string getATPRisingStar() {
	//HARDCODAM REZULTATE ANTERIOARE DIN LIPSA DE DATE
	std::vector<int> prevResults(101);
	srand(time(NULL));
	for (int i = 0; i <= 100; ++i) {
		prevResults[i] = rand() % 500 + 1;
	}

	ATPGetter atp;
	ParserATP pAtp;
	std::vector<Player> ATPplayers;
	std::string result;

	int currentMax = 0;

	pAtp.parsareDate();
	atp.getATPstats();

	std::ifstream ATPjson("ATP.json");
	nlohmann::json playerATP;

	while (true)
	{
		ATPjson >> playerATP;

		auto name = playerATP["name"];
		auto singelsWin = playerATP["singelsWins"];
		auto singelsLoss = playerATP["singelsLoss"];
		auto doubleWin = playerATP["doublesWins"];
		auto doubleLoss = playerATP["doublesLoss"];

		Player playerATPobj(name, singelsWin, singelsLoss, doubleWin, doubleLoss);
		ATPplayers.push_back(playerATPobj);

		if (playerATP["index"] == "100")
			break;
	}

	ATPjson.close();
	
	for (int i = 0; i < 100; ++i) {
		int currentPoints = ATPplayers[i].GetTotalPoints();
		if ((currentPoints - prevResults[i]) > currentMax)
		{
			currentMax = currentPoints - prevResults[i];
			result = ATPplayers[i].GetName();
		}
	}

	return result;
}

std::string getWTARisingStar() {
	//HARDCODAM REZULTATE ANTERIOARE DIN LIPSA DE DATE
	std::vector<int> prevResults(101);
	srand(time(NULL));
	for (int i = 0; i <= 100; ++i) {
		prevResults[i] = rand() % 500 + 1;
	}

	int currentMax = 0;

	WTAGetter wta;
	ParserWTA pWta;
	wta.getWTAstats();
	pWta.parsareDate();

	std::vector<Player> WTAplayers;
	std::string result;
	std::ifstream WTAjson("WTA.json");
	nlohmann::json playerWTA;

	while (true)
	{
		WTAjson >> playerWTA;

		auto name = playerWTA["name"];
		auto singelsWin = playerWTA["singelsWins"];
		auto singelsLoss = playerWTA["singelsLoss"];
		auto doubleWin = playerWTA["doublesWins"];
		auto doubleLoss = playerWTA["doublesLoss"];

		Player playerWTAobj(name, singelsWin, singelsLoss, doubleWin, doubleLoss);
		WTAplayers.push_back(playerWTAobj);

		if (playerWTA["index"] == "100")
			break;
	}

	WTAjson.close();

	for (int i = 0; i < 100; ++i) {
		int currentPoints = WTAplayers[i].GetTotalPoints();
		if ((currentPoints - prevResults[i]) > currentMax)
		{
			currentMax = currentPoints - prevResults[i];
			result = WTAplayers[i].GetName();
		}
	}

	return result;
}

std::string getRandomChancePlayersWTA() {
	std::string result;
	WTAGetter wta;
	ParserWTA pWta;
	wta.getWTAstats();
	pWta.parsareDate();

	std::vector<Player> WTAplayers;
	std::ifstream WTAjson("WTA.json");
	nlohmann::json playerWTA;

	while (true)
	{
		WTAjson >> playerWTA;

		auto name = playerWTA["name"];
		auto singelsWin = playerWTA["singelsWins"];
		auto singelsLoss = playerWTA["singelsLoss"];
		auto doubleWin = playerWTA["doublesWins"];
		auto doubleLoss = playerWTA["doublesLoss"];

		Player playerWTAobj(name, singelsWin, singelsLoss, doubleWin, doubleLoss);
		WTAplayers.push_back(playerWTAobj);

		if (playerWTA["index"] == "100")
			break;
	}

	WTAjson.close();
	RandomChanceWizard rnd(WTAplayers);
	rnd.RankPlayers();
	rnd.AddChance();
	return rnd.returnNewOrderByChance();
}

std::string getRandomChancePlayersATP() {
	ATPGetter atp;
	ParserATP pAtp;
	std::vector<Player> ATPplayers;
	std::string result;

	pAtp.parsareDate();
	atp.getATPstats();

	std::ifstream ATPjson("ATP.json");
	nlohmann::json playerATP;

	while (true)
	{
		ATPjson >> playerATP;

		auto name = playerATP["name"];
		auto singelsWin = playerATP["singelsWins"];
		auto singelsLoss = playerATP["singelsLoss"];
		auto doubleWin = playerATP["doublesWins"];
		auto doubleLoss = playerATP["doublesLoss"];

		Player playerATPobj(name, singelsWin, singelsLoss, doubleWin, doubleLoss);
		ATPplayers.push_back(playerATPobj);

		if (playerATP["index"] == "100")
			break;
	}

	ATPjson.close();
	RandomChanceWizard rnd(ATPplayers);
	rnd.RankPlayers();
	rnd.AddChance();
	return rnd.returnNewOrderByChance();
}

//PARTEA DE CONEXIUNE LA CLIENT

std::map<utility::string_t, utility::string_t> dictionary;

void displayInitialMessaje() {

	std::cout << "Welcome to the Tennis results application server" << std::endl;
}

void handle_get(http_request request)
{
	TRACE(L"\nhandle GET\n");

	dictionary.insert(std::pair<utility::string_t, utility::string_t>(utility::conversions::to_string_t("printSplash"),
		utility::conversions::to_string_t("WELCOME TO TENNIS LIVE!")));

	utility::string_t atpReq = utility::conversions::to_string_t("requestATPTop100");
	utility::string_t wtaReq = utility::conversions::to_string_t("requestWTATop100");
	utility::string_t rndAtpReq = utility::conversions::to_string_t("requestRandomChanceATP");
	utility::string_t rndWtaReq = utility::conversions::to_string_t("requestRandomChanceWTA");
	utility::string_t WRSTAR = utility::conversions::to_string_t("requestRStarWTA");
	utility::string_t ARSTAR = utility::conversions::to_string_t("requestRStarATP");

	std::string response = getTop100ATPplayers();

	dictionary.insert(std::pair<utility::string_t, utility::string_t>(atpReq,
		utility::conversions::to_string_t(response)));

	response = getTop100WTAplayers();

	dictionary.insert(std::pair<utility::string_t, utility::string_t>(wtaReq,
		utility::conversions::to_string_t(response)));
	
	response = getRandomChancePlayersATP();
	dictionary.insert(std::pair<utility::string_t, utility::string_t>(rndAtpReq,
		utility::conversions::to_string_t(response)));

	response = getRandomChancePlayersWTA();
	dictionary.insert(std::pair<utility::string_t, utility::string_t>(rndWtaReq,
		utility::conversions::to_string_t(response)));

	response = getATPRisingStar(); 
	dictionary.insert(std::pair<utility::string_t, utility::string_t>(ARSTAR,
		utility::conversions::to_string_t(response)));

	response = getWTARisingStar();
	dictionary.insert(std::pair<utility::string_t, utility::string_t>(WRSTAR,
		utility::conversions::to_string_t(response)));

	auto answer = json::value::object();

	for (auto const & p : dictionary)
	{
		answer[p.first] = json::value::string(p.second);
	}

	//display_json(json::value::null(), L"R: ");
	//display_json(answer, L"S: ");

	request.reply(status_codes::OK, answer);
}

void handle_request(
	http_request request,
	std::function<void(json::value const &, json::value &)> action)
{
	auto answer = json::value::object();

	request
		.extract_json()
		.then([&answer, &action](pplx::task<json::value> task) {
		try
		{
			auto const & jvalue = task.get();
			//display_json(jvalue, L"R: ");

			if (!jvalue.is_null())
			{
				action(jvalue, answer);
			}
		}
		catch (http_exception const & e)
		{
			std::wcout << e.what() << std::endl;
		}
	})
		.wait();


	//display_json(answer, L"S: ");

	request.reply(status_codes::OK, answer);
}


int main()
{
	displayInitialMessaje();

	TennisLiveGetter t;
	ParserTennisLiveResults pars;

	t.getTennisliveResults();

	dictionary.insert(pars.parsareDate());


	//STABILIM CONEXIUNEA LA CLIENT

	http_listener listener(L"http://localhost/tennisClient");	//localhost putem inlocui cu ip-ul clientului

	listener.support(methods::GET, handle_get);

	try
	{
		listener
			.open()
			.then([&listener]() {TRACE(L"\nstarting to listen\n"); })
			.wait();

		while (true);
	}
	catch (std::exception const & e)
	{
		std::wcout << e.what() << std::endl;
	}

	return 0;
}

