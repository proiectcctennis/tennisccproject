#pragma once
#include "Player.h"
#include<string>

class Ranking
{
private:
	std::vector<Player> players;

public:
	Ranking(const std::vector<Player>& players);
	~Ranking();

public:
	std::vector<Player> GetPlayers();
	std::vector<Player> GetSortedPlayers(const std::string& opt);
	std::string GetSortedPlayersAsString(const std::string& opt);

private:
	void Sort(std::vector<Player>& players);
};

