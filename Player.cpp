#include "Player.h"

Player::Player(const std::string n, const int sw, const int sl, const int dw, const int dl)
{
	name = n;
	singlesWin = sw;
	singlesLoss = sl;
	doublesWin = dw;
	doublesLoss = dl;

	singlesPoints = CalculateSinglesPoints();
	doublesPoints = CalculateDoublePoints();
	totalPoints = CalculateTotalPoints();

	singlesSelected = false;
	doublesSelected = false;
	totalSelected = false;
}

Player::~Player()
{

}

void Player::SetSinglesSelectedTrue()
{
	this->singlesSelected = true;
	this->doublesSelected = false;
	this->totalSelected = false;
}

void Player::SetDoublesSelectedTrue()
{
	this->doublesSelected = true;
	this->singlesSelected = false;
	this->totalSelected = false;
}

void Player::SetTotalSelectedTrue()
{
	this->singlesSelected = false;
	this->doublesSelected = false;
	this->totalSelected = true;
}

void Player::SetIndex(int ind)
{
	index = ind;
}

void Player::IncrementSinglesWin(int sw)
{
	singlesWin += sw;
}

void Player::IncrementSinglesLoss(int sl)
{
	singlesLoss += sl;
}

void Player::IncrementDoublesWin(int dw)
{
	doublesWin += dw;
}

void Player::IncrementDoublesLoss(int dl)
{
	doublesLoss += dl;
}

int Player::GetSinglesWin()
{
	return singlesWin;
}

int Player::GetSinglesLoss()
{
	return singlesLoss;
}

int Player::GetDoublesWin()
{
	return doublesWin;
}

int Player::GetDoublesLoss()
{
	return doublesLoss;
}

int Player::GetTotalWin()
{
	return singlesWin + doublesWin;
}

int Player::GetTotalLoss()
{
	return singlesLoss + doublesLoss;
}

int Player::GetIndex()
{
	return index;
}

std::string Player::GetName()
{
	return name;
}

float Player::GetSinglesPoints()
{
	return singlesPoints;
}

float Player::GetDoublesPoints()
{
	return doublesPoints;
}

float Player::GetTotalPoints()
{
	return totalPoints;
}

bool Player::operator<(const Player & player)
{
	if (this->singlesSelected == true) 
	{
		if (this->singlesPoints <= player.singlesPoints)
		{
			return true;
		}
		return false;
	}
	else if (this->doublesSelected == true) 
	{
		if (this->doublesPoints <= player.doublesPoints)
		{
			return true;
		}
		return false;
	}
	else 
	{
		if (this->totalPoints <= player.totalPoints)
		{
			return true;
		}
		return false;
	}
}

bool Player::operator>(const Player & player)
{
	if (this->singlesSelected == true)
	{
		if (this->singlesPoints >= player.singlesPoints)
		{
			return true;
		}
		return false;
	}
	else if (this->doublesSelected == true)
	{
		if (this->doublesPoints >= player.doublesPoints)
		{
			return true;
		}
		return false;
	}
	else
	{
		if (this->totalPoints >= player.totalPoints)
		{
			return true;
		}
		return false;
	}
}

float Player::CalculateSinglesPoints()
{
	float points;

	points = SINGLE_POINTS_CALCULUS * GetSinglesWin() + STARTING_POINTS;
	points -= SINGLE_POINTS_CALCULUS * GetSinglesLoss();

	return points;
}

float Player::CalculateDoublePoints()
{
	float points;

	points = DOUBLE_POINTS_CALCULUS * GetDoublesWin() + STARTING_POINTS;
	points -= DOUBLE_POINTS_CALCULUS * GetDoublesLoss();

	return points;
}

float Player::CalculateTotalPoints()
{
	return CalculateSinglesPoints() + CalculateDoublePoints() - STARTING_POINTS;
}

