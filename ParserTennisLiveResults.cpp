#include "ParserTennisLiveResults.h"
#include <cpprest/http_client.h>
#include <cpprest/filestream.h>
#include <json.hpp>

//#include<boost/algorithm/string/find.hpp>


std::pair<utility::string_t, utility::string_t> ParserTennisLiveResults::parsareDate()
{
	std::ifstream f("data.txt");

	std::string line, classType, player1name, player2name, set1, set2, set3, set4, set5, sets;
	std::string stResults = "";
	utility::string_t key = utility::conversions::to_string_t("getLastResults");
	utility::string_t results;// = utility::conversions::to_string_t(stResults);

	int countUntilLine = 0;

	bool reading = false;
	bool pairWaitingForSecondPlayer = false;
	bool readingStats = false;

	while (std::getline(f, line))
	{
		std::istringstream iss(line);

		if (countUntilLine > 0)
			countUntilLine++;

		if ((line.find("pair") != std::string::npos) || ( line.find("unpair") != std::string::npos))
		{
			reading = true;
			countUntilLine = 1;
		}

		if (line.find("tbody id") != std::string::npos)
		{
			if(!pairWaitingForSecondPlayer)
				reading = false;
			countUntilLine = 0;
			readingStats = false;
		}
		
		if (reading)
		{
			unsigned first;
			unsigned last;

			if (countUntilLine == 3 && (line.find("set act") == std::string::npos))
			{
				readingStats = true;
				if(pairWaitingForSecondPlayer == false){
					first = line.find(">");
					first = line.find(">", first + 1);
					last = line.find("</");
					player1name = line.substr(first + 1, last - first - 1);
					if (
						player1name.find("else tbody.remove();") == std::string::npos &&
						player1name.find("tbody=tbody_prev.next();") == std::string::npos &&
						player1name.find("{") == std::string::npos &&
						player1name.find("}") == std::string::npos)
					{
						stResults += "\n\n############\n\n";
						stResults += player1name;
						stResults += " :";
						pairWaitingForSecondPlayer = true;
					}
				}
			}

			//AL DOILEA JUCATOR 
			if (countUntilLine == 2 && (line.find("set act") == std::string::npos)) {
				if (pairWaitingForSecondPlayer == true) {
					first = line.find(">");
					first = line.find(">", first + 1);
					last = line.find("</");
					player2name = line.substr(first + 1, last - first - 1);
					if (player2name.find("else tbody.remove();") == std::string::npos &&
						player2name.find("tbody=tbody_prev.next();") == std::string::npos &&
						player2name.find("{") == std::string::npos &&
						player2name.find("}") == std::string::npos)
					{
						stResults += "\n\nVS\n\n";
						stResults += player2name;
						stResults += " ";
					}
					pairWaitingForSecondPlayer = false;
				}
			}

			if (countUntilLine == 4 && readingStats == true)
			{
				if (pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						sets = line.substr(first + 1, last - first - 1);
						stResults += "Sets :";
						stResults += sets;
						stResults += " ";
					}
				}
			}

			if (countUntilLine == 5 && readingStats == true)
			{
				if (pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						set1 = line.substr(first + 1, last - first - 1);
						stResults += "Set 1 :";
						stResults += set1;
						stResults += " ";
					}
				}
			}

			if (countUntilLine == 6 && readingStats == true)
			{
				if (pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						set2 = line.substr(first + 1, last - first - 1);
						stResults += "Set 2 :";
						stResults += set2;
						stResults += " ";
					}
				}
			}

			if (countUntilLine == 7 && readingStats == true)
			{
				if (pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						set3 = line.substr(first + 1, last - first - 1);
						stResults += "Set 3 :";
						stResults += set3;
						stResults += " ";
					}
				}
			}

			if (countUntilLine == 8 && readingStats == true)
			{
				if (pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						set4 = line.substr(first + 1, last - first - 1);
						stResults += "Set 4 :";
						stResults += set4;
						stResults += " ";
					}
				}
			}

			if (countUntilLine == 9 && readingStats == true)
			{
				if (pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						set5 = line.substr(first + 1, last - first - 1);
						stResults += "Set 5 :";
						stResults += set5;
					}
				}
			}

			//p2

			if (countUntilLine == 3 && readingStats == true)
			{
				if (!pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						sets = line.substr(first + 1, last - first - 1);
						stResults += "Sets :";
						stResults += sets;
						stResults += " ";
					}
				}
			}

			if (countUntilLine == 4 && readingStats == true)
			{
				if (!pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						set1 = line.substr(first + 1, last - first - 1);
						stResults += "Set 1 :";
						stResults += set1;
						stResults += " ";
					}
				}
			}

			if (countUntilLine == 5 && readingStats == true)
			{
				if (!pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						set2 = line.substr(first + 1, last - first - 1);
						stResults += "Set 2 :";
						stResults += set2;
						stResults += " ";
					}
				}
			}

			if (countUntilLine == 6 && readingStats == true)
			{
				if (!pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						set3 = line.substr(first + 1, last - first - 1);
						stResults += "Set 3 :";
						stResults += set3;
						stResults += " ";
					}
				}
			}

			if (countUntilLine == 7 && readingStats == true)
			{
				if (!pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						set4 = line.substr(first + 1, last - first - 1);
						stResults += "Set 4 :";
						stResults += set4;
						stResults += " ";
					}
				}
			}

			if (countUntilLine == 8 && readingStats == true)
			{
				if (!pairWaitingForSecondPlayer) {
					first = line.find(">");
					last = line.find("</");
					if (last - first > 1) {
						set5 = line.substr(first + 1, last - first - 1);
						stResults += "Set 5 :";
						stResults += set5;
					}
				}
			}


		}
	}

	f.close();

	results = utility::conversions::to_string_t(stResults);
	return std::pair<utility::string_t, utility::string_t>(key, results);
}
