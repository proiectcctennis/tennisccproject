#include "Match.h"

void Match::setPlayer1(Player & pl1)
{
	player1 = std::make_shared<Player>(pl1);
}

void Match::setPlayer2(Player & pl2)
{
	player2 = std::make_shared<Player>(pl2);
}
