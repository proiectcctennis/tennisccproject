#include "ParserWTA.h"
#include"json.hpp"

void ParserWTA::parsareDate()
{
	std::ifstream f("dataWTA.txt");
	std::string line, playerName;
	int countUntilLine = 0;
	int singelsWins, singelsLoss, singelsPct, doublesWins, doublesLoss, doublesPct;
	int playerIndex = 1;

	bool reading = false, doneReadingCurrentPlayerData = false;

	std::ofstream jsonFile("WTA.json");

	while (std::getline(f, line))
	{
		std::istringstream iss(line);

		if (line.find("atpRanking") != std::string::npos)	//cei de la tennis.com au refolosit codul html de la atp la wta :)
		{
			reading = true;
			doneReadingCurrentPlayerData = false;
		}

		if ((line.find("</tbody>") != std::string::npos) && reading == true)
		{
			reading = false;
			countUntilLine = 0;
		}

		if (countUntilLine > 0)
			countUntilLine++;

		if ((line.find("player-row") != std::string::npos) && reading == true)
		{
			countUntilLine = 1;
		}


		if (reading) {

			if (doneReadingCurrentPlayerData)
			{
				nlohmann::json player;
				player["index"] = std::to_string(playerIndex);
				player["name"] = playerName;
				player["singelsWins"] = singelsWins;
				player["singelsLoss"] = singelsLoss;
				player["singelsPct"] = singelsPct;
				player["doublesWins"] = doublesWins;
				player["doublesLoss"] = doublesLoss;
				player["doublesPct"] = doublesPct;
				jsonFile << player << std::endl;
				playerIndex++;
				doneReadingCurrentPlayerData = false;
			}

			if (countUntilLine == 5)
			{
				const auto strBegin = line.find_first_not_of(' ');
				playerName = line.substr(strBegin);
				//std::cout << playerName << std::endl;
			}
			if (countUntilLine >= 14 && countUntilLine <= 19)
			{
				std::string sWins, sLoss, sPct, dWins, dLoss, dPct;
				unsigned first;
				unsigned last;
				switch (countUntilLine) {
				case 14:
					first = line.find('>');
					last = line.find("</");
					sWins = line.substr(first + 1, last - first - 1);
					singelsWins = std::stoi(sWins);
					//std::cout << "singelsWins: " << singelsWins << std::endl;
					break;
				case 15:
					first = line.find('>');
					last = line.find("</");
					sLoss = line.substr(first + 1, last - first - 1);
					singelsLoss = std::stoi(sLoss);
					//std::cout << "singelsLoss: " << singelsLoss << std::endl;
					break;
				case 16:
					first = line.find('>');
					last = line.find("</");
					sPct = line.substr(first + 1, last - first - 1);
					singelsPct = std::stoi(sPct);
					//std::cout << "singelsPct: " << singelsPct << std::endl;
					break;
				case 17:
					first = line.find('>');
					last = line.find("</");
					dWins = line.substr(first + 1, last - first - 1);
					doublesWins = std::stoi(dWins);
					//std::cout << "doublesWins: " << doublesWins << std::endl;
					break;
				case 18:
					first = line.find('>');
					last = line.find("</");
					dLoss = line.substr(first + 1, last - first - 1);
					doublesLoss = std::stoi(dLoss);
					//std::cout << "doublesLoss: " << doublesLoss << std::endl;
					break;
				case 19:
					first = line.find('>');
					last = line.find("</");
					dPct = line.substr(first + 1, last - first - 1);
					doublesPct = std::stoi(dPct);
					//std::cout << "doublesPct: " << doublesPct << std::endl << std::endl;
					doneReadingCurrentPlayerData = true;
					break;
				}

			}
		}
	}

	jsonFile.close();
}
